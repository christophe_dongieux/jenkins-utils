#!groovy

import com.netgem.notification.GoogleChatNotification
import com.netgem.notification.SlackNotification

def call(final Map params) {
    List<Map> servicesConfigs = params.servicesConfigs
    Map message = params.message
    if (servicesConfigs) {
        for (Map serviceConfig : servicesConfigs) {
            dispatch(serviceConfig, message)
        }
    } else {
        Map serviceConfig = params.serviceConfig
        dispatch(serviceConfig, message)
    }
}

def dispatch(final Map serviceConfig, final Map message) {
    String serviceName = serviceConfig.name
    switch (serviceName) {
        case 'Slack':
            slack(serviceConfig, message)
            break
        case 'GoogleChat':
            googleChat(serviceConfig, message)
            break
    }
}

def slack(final Map serviceConfig, final Map message) {
    def messageType = message.type
    if (messageType) {
        slackBuildMessageForType(messageType, message)
    }
    httpRequest(contentType: 'APPLICATION_JSON', httpMode: 'POST', ignoreSslErrors: true, quiet: true, requestBody: SlackNotification.fromMessage(this, message), responseHandle: 'NONE', url: serviceConfig.webHookUrl)
}

def slackBuildMessageForType(def messageType, Map message) {
    def text = null
    def sections = []
    switch (messageType) {
        case 'failure':
            text = ":x: *${env.JOB_NAME}* pipeline failed! :x:"
            sections = [[color: 'danger', widgets: [[titleValue: [label: "Build number", text: "${env.BUILD_DISPLAY_NAME}"]], [button: [text: 'Go to build', url: "${env.BUILD_URL}"]]]]]
            break
        case 'fixed':
            text = "Back to normal :heavy_check_mark:"
            break
        default:
            text = messageType
            break
    }
    if (text) {
        message['text'] = text
    }
    if (!sections.isEmpty()) {
        message['sections'] = sections
    }
}

def googleChat(final Map serviceConfig, final Map message) {
    def messageType = message.type
    if (messageType) {
        googleChatBuildMessageForType(messageType, message)
    }
    httpRequest(contentType: 'APPLICATION_JSON', httpMode: 'POST', ignoreSslErrors: true, quiet: true, requestBody: GoogleChatNotification.fromMessage(this, message), responseHandle: 'NONE', url: serviceConfig.webHookUrl)
}

def googleChatBuildMessageForType(def messageType, Map message) {
    def text = null
    def sections = []
    switch (messageType) {
        case 'failure':
            text = "❌ <b>${env.JOB_NAME}</b> pipeline failed! ❌"
            sections = [[widgets: [[titleValue: [label: "Build number", text: "${env.BUILD_DISPLAY_NAME}"]], [button: [text: 'Go to build', url: "${env.BUILD_URL}"]]]]]
            break
        case 'fixed':
            text = "Back to normal ✔"
            break
        default:
            text = messageType
            break
    }
    if (text) {
        message['text'] = text
    }
    if (!sections.isEmpty()) {
        message['sections'] = sections
    }
}

/*call([serviceConfig: [name: 'Slack'],
      message      : [
              username   : 'Foobar',
              text       : 'My text',
              sections: [
                      [color : 'danger',
                       widgets: [
                               [title: 'Field title 1', value: 'Field value 1', isShort: true],
                               [title: 'Field title 2', value: 'Field value 2']
                       ]
                      ]
              ]
      ]
])

call([serviceConfig: [name: 'Slack'],
      message      : [
              username   : 'Foobar',
              text       : 'My text',
              sections: null
      ]
])*/

/*env = [
    JOB_NAME: "Test job name",
    BUILD_DISPLAY_NAME: "Test build name",
    BUILD_URL: "Test build URL",
]

echo = Script.&println as Closure

call([serviceConfig: [name: 'Slack', webHookUrl: "https://www.google.com"],
      message: [username: 'test', type: 'failure']])

call([serviceConfig: [name: 'GoogleChat', webHookUrl: "https://www.google.com"],
      message: [username: 'test', type: 'failure']])*/