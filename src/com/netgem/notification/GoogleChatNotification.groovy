package com.netgem.notification

import groovy.json.JsonOutput

class GoogleChatNotification implements Serializable {

    private static final String TAG = "GoogleChatNotification"
    private static final boolean DEBUG = false

    static String fromMessage(Script script, Map message = [:]) {
        if (DEBUG) script.echo "$TAG: message=$message"
        String text = message.get('text', '') as String
        List<Map> msgSections = message.get('sections', []) as List<Map>
        def sections = []
        for (Map msgSection : msgSections) {
            if (DEBUG) script.echo "$TAG: msgSection=$msgSection"
            List<Map> msgWidgets = msgSection.get('widgets', []) as List<Map>
            def widgets = []
            for (Map msgWidget : msgWidgets) {
                if (DEBUG) script.echo "$TAG: msgWidget=$msgWidget"
                def widgetType = msgWidget.keySet().first()
                Map widgetConfig = msgWidget.get(widgetType) as Map
                def widget
                switch (widgetType) {
                    case 'titleValue':
                        String title = widgetConfig.get('label', null) as String
                        String value = widgetConfig.get('text', null) as String
                        widget = [keyValue: [topLabel: title, content: value]]
                        break
                    case 'button':
                        String title = widgetConfig.get('text', null) as String
                        String url = widgetConfig.get('url', null) as String
                        widget = [buttons: [[textButton: [text: title, onClick: [openLink: [url: url]]]]]]
                        break
                }
                if (widget) {
                    widgets << widget
                }
            }
            sections << [widgets: widgets]
        }
        def output = [cards: [[header: [title: text], sections: sections]]]
        if (DEBUG) script.echo "$TAG: json=" + JsonOutput.toJson(output)
        return JsonOutput.toJson(output)
    }
}
