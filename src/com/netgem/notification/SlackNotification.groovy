package com.netgem.notification

@Grab('com.google.code.gson:gson:2.8.5')

import com.google.gson.JsonArray
import com.google.gson.JsonObject

class SlackNotification implements Serializable {

    private static final String TAG = "SlackNotification"
    private static final boolean DEBUG = false

    static String fromMessage(Script script, Map message = [:]) {
        if (DEBUG) script.echo "$TAG: message=$message"
        String username = message.get('username', null) as String
        String text = message.get('text', '') as String
        JsonObject notification = new JsonObject()
        notification.addProperty('username', username)
        notification.addProperty('text', text)
        JsonArray attachments = new JsonArray()
        List<Map> msgSections = message.get('sections', []) as List<Map>
        for (Map msgSection : msgSections) {
            if (DEBUG) script.echo "$TAG: msgSection=$msgSection"
            String color = msgSection.get('color', null) as String
            JsonObject attachment = new JsonObject()
            attachment.addProperty('color', color)
            attachments.add(attachment)
            JsonArray fields = new JsonArray()
            JsonArray actions = new JsonArray()
            List<Map> msgWidgets = msgSection.get('widgets', []) as List<Map>
            for (Map msgWidget : msgWidgets) {
                if (DEBUG) script.echo "$TAG: msgWidget=$msgWidget"
                def widgetType = msgWidget.keySet().first()
                Map widgetConfig = msgWidget.get(widgetType) as Map
                JsonObject field = new JsonObject()
                switch (widgetType) {
                    case 'titleValue':
                        String title = widgetConfig.get('label', null) as String
                        String value = widgetConfig.get('text', null) as String
                        boolean isShort = widgetConfig.get('isShort', false)
                        field.addProperty('title', title)
                        field.addProperty('value', value)
                        field.addProperty('short', isShort)
                        break
                    case 'button':
                        String title = widgetConfig.get('text', null) as String
                        String url = widgetConfig.get('url', null) as String
                        JsonObject action = new JsonObject()
                        action.addProperty('type', 'button')
                        action.addProperty('text', title)
                        action.addProperty('url', url)
                        actions.add(action)
                        break
                }
                fields.add(field)
            }
            attachment.add('fields', fields)
            attachment.add('actions', actions)
        }
        notification.add('attachments', attachments)
        String json = notification.toString()
        if (DEBUG) script.echo "$TAG: json=$json"
        return json
    }
}
