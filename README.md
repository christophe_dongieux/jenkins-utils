# Jenkins Shared Library
This is the Netgem shared library for Jenkins.
See [documentation](https://jenkins.io/doc/book/pipeline/shared-libraries/).

# Notification helper
This helper is intended to provide Instant Messaging notifications for your Jenkins pipelines.
For now, this helper supports:

* Slack

* Google Chat

## Usage
You can use this helper from your Jenkins declarative pipelines:

```groovy
@Library('netgem-jenkins-utils') _ // <-- the underscore is on purpose!

pipeline {
	environment {
		slackUrl = 'https://hooks.slack.com/services/Txxxxxxxx/Byyyyyyyy/zzzzzzzzzzzzzzz'
		googleChatUrl = 'https://chat.googleapis.com/v1/spaces/...'
	}

	...

	post {
		failure {
			notification servicesConfigs: [[name: 'Slack', webHookUrl: "${slackUrl}"], [name: 'GoogleChat', webHookUrl: "${googleChatUrl}"]], message: [username: 'Jenkins', type: 'failure']
		}
	}
}
```

## Message format
### Predefined types
The `notification` helper has some built-in predefined notifications. It's quite handy to use this instead of defining the whole message for each notification to send.

#### Failure
```groovy
notification servicesConfig: ..., message: [username: 'Jenkins', type: 'failure']
```

On Slack:

![Slack notification](doc/slack_failure_notification.png)

On Google Chat:

![Google Chat notification](doc/google_chat_failure_notification.png)

#### Fixed
```groovy
notification servicesConfig: ..., message: [username: 'Jenkins', type: 'fixed']
```

On Slack:

![Slack notification](doc/slack_fixed_notification.png)

On Google Chat:

![Google Chat notification](doc/google_chat_fixed_notification.png)

### Custom
A custom message format has this format:
```json
"text": "My notification"
"sections": [
	{
		"color": "#rrggbb"
		"text": "Section text"
		"widgets": [
			{
				"titleValue": {
					"label": "My label"
					"text": "My label value"
				}
			},
			{
				"button": {
					"text": "My button text",
					"url": "My button URL"
				}
			},
			...
		]
	},
	...
]
```

Here is an example of the message format for the `failure` predefined type:
```groovy
[text: ":x: *${env.JOB_NAME}* pipeline failed! :x:",
 sections: [
	 [color: 'danger',
	 widgets: [
		 [titleValue: [label: "Build number", text: "${env.BUILD_DISPLAY_NAME}"]],
		 [button: [text: 'Go to build', url: "${env.BUILD_URL}"]]
	]]
]]
```

## Slack
There are 2 ways to send Slack notifications:

* From Jenkins with the [Slack Notification plugin](https://plugins.jenkins.io/slack) and Slack's Jenkins integration.

* With [Slack Incoming WebHooks](https://my.slack.com/apps/manage/custom-integrations)

This helper uses the second option to send notifications.

### WebHook URL retrieval
Procedure to retrieve a Slack channel WebHook URL:

1. Go to [Slack Incoming WebHooks](https://my.slack.com/apps/manage/custom-integrations)

2. Add a new Configuration or edit an existing one[^1]

3. Go to **Setup Instructions**

4. Copy the **WebHook URL**[^2]

[^1] [Slack Incoming WebHooks Configuration](doc/slack_incoming_webhooks_1.png)

[^2] [Slack Incoming WebHooks URL](doc/slack_incoming_webhooks_2.png)

### Notification `serviceConfig`
```groovy
notification servicesConfigs: [[name: 'Slack', webHookUrl: <the WebHook URL>], ...], ...
```

## Google Chat
Send a notification in a specific [Google Chat](https://chat.google.com) room.

### WebHook URL retrieval
Procedure to retrieve a Google Chat room WebHook URL:

1. Go to [the Google Chat room](https://chat.google.com/u/0/room/XXXXXXX) you want your notifications to be displayed

2. Click on the room name on top of the page[^1]

3. Click on **Configure webhooks**

4. Add a new webhook for Jenkins[^2]

[^1] [Google Chat WebHooks Configuration](doc/google_chat_incoming_webhooks_1.png)

[^2] [Google Chat WebHooks URL](doc/google_chat_incoming_webhooks_2.png)

### Notification `serviceConfig`
```groovy
notification servicesConfigs: [[name: 'GoogleChat', webHookUrl: <the WebHook URL>], ...], ...
```